import {
  ArrowUpCircleIcon,
  ClockIcon,
  FilterIcon,
  FlagIcon,
  HeartIcon,
  MessageCircleIcon,
  MoreVerticalIcon,
  MoveVerticalIcon,
  PackagePlusIcon,
} from "lucide-react";
import React from "react";

export default function HomePage() {
  return (
    <>
      <div className="fixed  top-0 flex w-full items-center gap-2 border-b bg-white p-2 md:w-[360px] md:m-auto">
        <input
          placeholder="Search"
          className="h-9 w-full rounded-full border border-gray-300"
        />
        <div className="w-8 text-gray-500">
          <FilterIcon size={20} />
        </div>
      </div>
      <div className="mt-12">
        <Post />
        <Post />
        <Post />
        <Post />
        <Post />
        <Post />
        <Post />
      </div>
    </>
  );
}

const Post = () => {
  return (
    <div className="flex w-full flex-col">
      <div className="flex h-[60px] w-full  items-center gap-2 px-3">
        <div>
          <img
            src={"https://picsum.photos/500?random=3"}
            className="h-6 w-6 rounded-full"
          />
        </div>

        <div className="flex w-full flex-col items-start gap-0">
          <p className="text-sm font-bold">Dana Indrajaya</p>
          <div className="flex gap-1">
            <div className="h-3 rounded-md bg-gray-600 px-1 text-[10px] leading-3 text-white">
              Starter
            </div>
            <div className="h-3 rounded-md bg-yellow-400 px-1 text-[10px] font-bold leading-3 text-gray-800">
              VIP
            </div>
          </div>
        </div>
        <div className="text-gray-500">
          <MoreVerticalIcon />
        </div>
      </div>
      <div className="w-screen md:w-[360px] md:m-auto flex flex-nowrap overflow-auto">
        <img src="https://picsum.photos/1000?random=1" className="w-screen md:w-[360px] md:m-auto " />
        <img src="https://picsum.photos/1000?random=5" className="w-screen md:w-[360px] md:m-auto" />
        <img src="https://picsum.photos/1000?random=17" className="w-screen md:w-[360px] md:m-auto" />
        <img src="https://picsum.photos/1000?random=3" className="w-screen md:w-[360px] md:m-auto" />
        <img src="https://picsum.photos/1000?random=4" className="w-screen md:w-[360px] md:m-auto" />
      </div>
      <div className="flex w-full items-center gap-3 p-3 text-gray-500">
        <HeartIcon size={20} />

        <MessageCircleIcon size={20} />
        <FlagIcon size={20} />
        <div className="flex-grow">&nbsp;</div>
        <button className="flex items-center gap-1 rounded-full border border-red-500 p-1 px-2 text-red-500">
          <ClockIcon size={14} />
          <p className="text-xs">2D:12:06:58</p>
        </button>
        {/* <PackagePlusIcon size={20} /> */}
      </div>

      <div className="flex w-full flex-col gap-1 px-3 text-xs">
        <div className="font-bold">Lelang Mini Gt Supra TRD 3000</div>
        <div>Kondisi loose mulus</div>
        <div className="flex gap-2">
          <div className="w-5">OB </div>
          <div>Rp 200.000</div>
        </div>
        <div className="flex gap-2">
          <div className="w-5">NB </div>
          <div>Rp 5.000</div>
        </div>
        <div className="flex gap-2">
          <div className="w-5">BIN </div>
          <div>Rp.350.000</div>
        </div>
      </div>
      <div className="flex">
        <div className="mb-6 mt-4 flex w-full  flex-col gap-1 px-3 text-xs text-gray-500">
          <div className="font-bold text-gray-800 ">Bid Status</div>
          <div className="flex gap-2">
            <div className="w-1/5">Rp. 275.000 </div>
            <div>Ali Askari</div>
          </div>
          <div className="flex gap-2">
            <div className="w-1/5">Rp. 285.000 </div>
            <div>Tanqiyah</div>
          </div>
          <div className="flex gap-2">
            <div className="w-1/5">Rp. 290.000 </div>
            <div>
              Radit Yanuar <span className="text-gray-800">(highest)</span>
            </div>
          </div>
        </div>
        {/* <div className="px-3">
          <button className="mt-4 flex items-center gap-1 rounded-lg  bg-green-500 p-3 text-white">
            <ArrowUpCircleIcon size={20} />
            <p className="text-base">Join</p>
          </button>
        </div> */}
      </div>
    </div>
  );
};

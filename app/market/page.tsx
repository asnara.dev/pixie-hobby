import { ShoppingCartIcon } from "lucide-react";
import React from "react";

export default function HomePage() {
  return (
    <div className="flex w-full flex-col">
      <div className="fixed left-0 top-0 flex w-full gap-2 border-b bg-white p-4">
        <button className="rounded-full border bg-sky-200 px-4 py-2 text-sm text-gray-800">
          All
        </button>
        <button className="rounded-full border bg-gray-200 px-4 py-2 text-sm text-gray-500">
          WTB
        </button>
        <button className="rounded-full border bg-gray-200 px-4 py-2 text-sm text-gray-500">
          WTS
        </button>
      </div>
      <div className="mb-[96px] mt-[72px] flex w-full flex-col">
        <ItemWTS />
        <ItemWTS />
        <ItemWTS />
        <ItemWTS />
        <ItemWTS />
        <ItemWTS />
        <ItemWTS />
        <ItemWTS />
        <ItemWTS />
        <ItemWTS />
        <ItemWTS />
        <ItemWTS />
        <ItemWTS />
      </div>
    </div>
  );
}

const ItemWTS = () => {
  return (
    <div className="flex w-full flex-nowrap items-center gap-2 border-b p-2">
      <div className="h-20 w-20 border bg-gray-200">
        <img
          src={"https://picsum.photos/500?random=3"}
          className="h-full w-full"
        />
      </div>
      <div className="flex flex-grow flex-col flex-wrap">
        <div className="w-[calc(100vw_-_200px)] text-sm font-bold">
          Item name Item name Item Item name Item name
        </div>
        <div className="mb-2 text-xs">Seller name</div>
        <div className="text-xs font-bold text-orange-500">Rp. 99.000</div>
      </div>
      <div>
        <button className="flex w-20 flex-col items-center justify-center rounded bg-green-400 py-4 text-white">
          <ShoppingCartIcon size={20} />
          <p>Buy</p>
        </button>
      </div>
    </div>
  );
};

"use client";
import {
  ActivityIcon,
  DollarSignIcon,
  HomeIcon,
  MenuIcon,
  ShoppingBagIcon,
} from "lucide-react";
import { usePathname } from "next/navigation";
import React from "react";

import BottomBarItem from "./bottombar-item";
import MainMenu from "./main-menu";

export default function BottomBar() {
  const pathname = usePathname();
  console.log(pathname.includes("home"));

  return (
    <div className="fixed bottom-4 w-[calc(100vw_-_32px)] flex rounded-full mx-4 justify-around border bg-white md:w-[360px] md:m-auto">
      <BottomBarItem
        label="Home"
        url="/home"
        icon={<HomeIcon size={16} />}
        active={pathname.includes("home") || pathname === "/"}
      />
      <BottomBarItem
        label="Market"
        url="/market"
        icon={<ShoppingBagIcon size={16} />}
        active={pathname.includes("market")}
      />
      <BottomBarItem
        url="/auction"
        label="Auction"
        icon={<DollarSignIcon size={16} />}
        active={pathname.includes("auction")}
      />
      <BottomBarItem
        label="Activity"
        url="/activity"
        icon={<ActivityIcon size={16} />}
        active={pathname.includes("activity")}
      />
      <MainMenu />
    </div>
  );
}

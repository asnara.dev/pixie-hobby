import cx from "classnames";
import { HomeIcon } from "lucide-react";
import Link from "next/link";
const BottomBarItem = ({
  label = "Home",
  url = "/",
  active = false,
  icon = <HomeIcon color="gray" size={12} />,
}) => {
  return (
    <Link
      href={url}
      className={cx(
        "m-2 flex flex-grow cursor-pointer relative flex-col items-center gap-1 rounded-full p-2 text-gray-500",
        active && "bg-white text-gray-900",
      )}
    >
      <div>{icon}</div>
      <p className="text-[10px] ">{label}</p>
      {active && <div className="absolute block bottom-0 h-1 w-4 rounded-full bg-sky-500"></div>}
    </Link>
  );
};

export default BottomBarItem;

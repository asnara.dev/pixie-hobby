"use client";
import {
  ActivityIcon,
  AlarmPlusIcon,
  HomeIcon,
  InboxIcon,
  MenuIcon,
  PackageIcon,
  PackagePlusIcon,
  PlusCircleIcon,
  PlusIcon,
  SettingsIcon,
  ShoppingCartIcon,
  UserIcon,
  WalletIcon,
  XIcon,
} from "lucide-react";
import BottomBarItem from "./bottombar-item";
import * as Popover from "@radix-ui/react-popover";
import cx from "classnames";
import Link from "next/link";

const MainMenu = () => {
  return (
    <Popover.Root>
      <Popover.Trigger asChild>
        <button
          className="m-2 flex flex-grow cursor-pointer flex-col items-center gap-1 rounded-md p-2 text-gray-500"
          aria-label="Update dimensions"
        >
          <MenuIcon size={16} />
          <p className="text-[10px] ">Menu</p>
        </button>
      </Popover.Trigger>
      <Popover.Portal>
        <Popover.Content
          className="data-[state=open]:data-[side=bottom]:animate-slideUpAndFade data-[state=open]:data-[side=left]:animate-slideRightAndFade z-50 mr-5 max-h-[70vh] w-[300px] overflow-y-auto overflow-x-hidden rounded-xl bg-white shadow-[0_10px_38px_-10px_hsla(206,22%,7%,.35),0_10px_20px_-15px_hsla(206,22%,7%,.2)] will-change-[transform,opacity] focus:shadow-[0_10px_38px_-10px_hsla(206,22%,7%,.35),0_10px_20px_-15px_hsla(206,22%,7%,.2),0_0_0_2px_theme(colors.violet7)] data-[state=open]:data-[side=right]:animate-slide-up-fade data-[state=open]:data-[side=top]:animate-slide-down-fade"
          sideOffset={20}
        >
          <div className="right-6 grid grid-cols-4 gap-2 p-4">
            <MainMenuItem
              href="#"
              className="col-span-4 bg-blue-400 text-white"
              label="Create a New Post"
              icon={<PlusCircleIcon size={24} />}
            />
            <MainMenuItem
              href="#"
              className="col-span-4"
              label="Sell an Item"
              icon={<PackagePlusIcon size={24} />}
            />
            <MainMenuItem
              href="#"
              className="col-span-4"
              label="Create an Auction"
              icon={<AlarmPlusIcon size={24} />}
            />
            <MainMenuItem href="#" badge="99+" icon={<InboxIcon size={24} />} />
            <MainMenuItem href="#" icon={<ShoppingCartIcon size={24} />} />
            <MainMenuItem href="#" icon={<UserIcon size={24} />} />
            <MainMenuItem href="#" icon={<WalletIcon size={24} />} />
            <MainMenuItem
              href="#"
              label="Activity"
              className="col-span-2"
              icon={<ActivityIcon size={24} />}
            />
            <MainMenuItem
              href="#"
              label="Catalog"
              className="col-span-2"
              icon={<PackageIcon size={24} />}
            />
            <MainMenuItem
              href="#"
              label="Setting"
              className="col-span-4"
              icon={<SettingsIcon size={24} />}
            />
          </div>

          {/* <Popover.Close
            className="text-violet11 hover:bg-violet4 focus:shadow-violet7 absolute right-[5px] top-[5px] inline-flex h-[25px] w-[25px] cursor-default items-center justify-center rounded-full outline-none focus:shadow-[0_0_0_2px]"
            aria-label="Close"
          >
            <XIcon />
          </Popover.Close> */}
          <Popover.Arrow className="fill-white" />
        </Popover.Content>
      </Popover.Portal>
    </Popover.Root>
  );
};

export default MainMenu;

const MainMenuItem = ({
  badge = "",
  className = "",
  href = "",
  label = "",
  icon = <HomeIcon />,
}) => {
  return (
    <Link
      className={cx(
        "relative col-span-1 flex items-center justify-center gap-2 rounded-md border py-4",
        className,
      )}
      href={href}
    >
      {badge && (
        <div className="absolute -right-2 -top-2 rounded-md border bg-red-500 px-1 text-center text-xs text-white">
          {badge}
        </div>
      )}
      {icon}
      {label && <p className="text-xs">{label}</p>}
    </Link>
  );
};
